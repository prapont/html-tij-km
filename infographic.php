<?php include('inc/header.php'); ?>

<div class="breadcrumbs">
	<div class="container"><a href="index.php">หน้าหลัก</a><span>อินโฟกราฟิก</span></div>
</div>

<section class="section section-article-list">
	<div class="container">
		<h2 class="section-title">อินโฟกราฟิก</h2> 
		<div class="article-filter">
			จัดเรียง 
			<select name="" class="form-select">
				<option value="">บทความล่าสุด</option>
				<option value="">บทความ 30 วันที่ผ่านมา</option>
			</select>
		</div>
		<div class="columns">
			<div class="column col-6 col-lg-6 col-sm-12">
				<div class="card card-article card-highlight-article">
					<a href="infographic-detail.php"></a>
					<div class="card-image" style="background-image:url('assets/img/article/infographic/01.jpg');"></div>
					<div class="card-content">
						<div class="hashtag">#อินโฟกราฟิก</div>
						<h3 class="card-title">Women Access to Justice - MAR 2018</h3>
						<div class="card-footer">
							<p class="date"><i class="icon-calendar"></i> 23.01.2562</p>
							<a class="readmore" href="">อ่านต่อ <i class="icon-angle-right"></i></a>
						</div>
					</div>
				</div>
			</div>
			<?php for($i=0;$i<=5;$i++) { ?>
			<?php 
				$title = array("Violence Cycle - NOV 2017","IBR From Victim to Convict - NOV 2017","Violence Opinion - NOV 2017","SUSO Rape Media - NOV 2017","Rape Stat - NOV 2017","SUSO รักนวลสงวนสิทธิ์ - NOV 2017");
			?>
			<div class="column col-3 col-lg-6 col-sm-12">
				<div class="card card-article">
					<div class="card-image">
						<a href="infographic-detail.php" class="hover-img"><img src="assets/img/article/infographic/0<?php echo $i+1;?>.jpg" class="img-responsive"></a>
					</div>
					<div class="card-header">
						<div class="hashtag">#อินโฟกราฟิก</div>
						<h3 class="card-title"><?php echo $title[$i]; ?></h3>
					</div>
					<div class="card-footer">
						<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
						<a href="">อ่านต่อ</a>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		
		<div class="article-footer">
			<div class="page-counter">
				หน้า 01/14
			</div>
			<div class="pagination">
				<a href="#"><i class="icon icon-angle-left"></i> ย้อนกลับ</a>
				<a class="active" href="#">1</a>
				<a href="#">2</a>
				<a href="#">3</a>
				<a href="#">ต่อไป <i class="icon icon-angle-right"></i></a>
			</div>
			<div class="page-jump">
				กดเพื่อดูหน้า
				<input type="text" class="form-input" placeholder="1">
				<button class="btn">กด</button>
			</div>
		</div>
	</div>
</section>

<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>