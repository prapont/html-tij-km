<?php include('inc/header.php'); ?>

<div class="breadcrumbs">
	<div class="container"><a href="index.php">หน้าหลัก</a><span>งานวิจัย</span></div>
</div>

<section class="section section-article-list">
	<div class="container">
		<h2 class="section-title">งานวิจัย</h2> 
		<div class="article-filter">
			จัดเรียง 
			<select name="" class="form-select">
				<option value="">งานวิจัยล่าสุด</option>
				<option value="">งานวิจัย 30 วันที่ผ่านมา</option>
			</select>
		</div>
		<div class="columns">
			<?php for($i=0;$i<=6;$i++) { ?>
			<?php 
				$title = array("Women's Pathways to Prison in Kenya","เพศภาวะและเส้นทางสู่เรือนจำ","Victimization Experiences Among Young People In Custody : Systematic Review pf Methods and Tools","Violence against Children Survey","The Trial of Rape","Trafficking in persons from Cambodia, Lao PDR and Myanmar to Thailand","Criminal Justice Performance Indicators Expected Values and Gaps");
			?>
			<div class="column col-3 col-lg-6 col-sm-12">
				<div class="card card-article">
					<div class="card-image">
						<a href="publication-detail.php" class="hover-img">
							<img src="assets/img/article/research/0<?php echo $i+1;?>.jpg" class="img-responsive">
						</a>
					</div>
					<div class="card-header">
						<div class="hashtag">#สิ่งพิมพ์</div>
						<h3 class="card-title"><?php echo $title[$i]; ?></h3>
					</div>
					<div class="card-footer">
						<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="article-footer">
			<div class="page-counter">
				หน้า 01/14
			</div>
			<div class="pagination">
				<a href="#"><i class="icon icon-angle-left"></i> ย้อนกลับ</a>
				<a class="active" href="#">1</a>
				<a href="#">2</a>
				<a href="#">3</a>
				<a href="#">ต่อไป <i class="icon icon-angle-right"></i></a>
			</div>
			<div class="page-jump">
				กดเพื่อดูหน้า
				<input type="text" class="form-input" placeholder="1">
				<button class="btn">กด</button>
			</div>
		</div>
	</div>
</section>

<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>