<?php include('inc/header.php'); ?>

<div class="breadcrumbs">
	<div class="container"><a href="index.php">หน้าหลัก</a><span>เอกสารวิชาการ</span></div>
</div>

<section class="section section-article-list">
	<div class="container">
		<h2 class="section-title">เอกสารวิชาการ</h2> 
		<div class="article-filter">
			จัดเรียง 
			<select name="" class="form-select">
				<option value="">เอกสารวิชาการล่าสุด</option>
				<option value="">เอกสารวิชาการ 30 วันที่ผ่านมา</option>
			</select>
		</div>
		<div class="columns">
			<?php for($i=0;$i<=6;$i++) { ?>
			<?php 
				$title = array("The Model Prison","Assessment Tool - ข้อกำหนดแมนเดลา","EXPO 1st Seminar Report Community","TIJ Booklet 2018 on Dev Led Crime Prevention","Bangkok Dialogue RoL","Bangkok Rules Training","EXPO Pocket Guide");
			?>
			<div class="column col-3 col-lg-6 col-sm-12">
				<div class="card card-article">
					<div class="card-image">
						<a href="publication-detail.php" class="hover-img">
							<img src="assets/img/article/publication/0<?php echo $i+1;?>.jpg" class="img-responsive">
						</a>
					</div>
					<div class="card-header">
						<div class="hashtag">#เอกสารวิชาการ</div>
						<h3 class="card-title"><?php echo $title[$i]; ?></h3>
					</div>
					<div class="card-footer">
						<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="article-footer">
			<div class="page-counter">
				หน้า 01/14
			</div>
			<div class="pagination">
				<a href="#"><i class="icon icon-angle-left"></i> ย้อนกลับ</a>
				<a class="active" href="#">1</a>
				<a href="#">2</a>
				<a href="#">3</a>
				<a href="#">ต่อไป <i class="icon icon-angle-right"></i></a>
			</div>
			<div class="page-jump">
				กดเพื่อดูหน้า
				<input type="text" class="form-input" placeholder="1">
				<button class="btn">กด</button>
			</div>
		</div>
	</div>
</section>

<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>