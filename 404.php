<?php include('inc/header.php'); ?>

<div class="page-404">
	<div class="container">
		<img src="assets/img/404.png" alt="">
		<p>ขออภัย!
			<span>ไม่พบหน้าที่คุณต้องการ.</span>
		</p>
		<a href="index.php" class="btn"><i class="icon icon-angle-left"></i> กลับสู่หน้าหลัก</a>
	</div>
</div>

<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>