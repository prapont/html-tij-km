<?php include('inc/header.php'); ?>

<div class="breadcrumbs">
	<div class="container"><a href="index.php">หน้าหลัก</a><span>ค้นหา</span></div>
</div>

<section class="section section-search-result">
	<div class="container">
		<h2 class="section-title">ค้นหา</h2> 

		<div class="search-result">
			<h2 class="text-grey">คำค้นหา : "ประชุม"</h2>
			<p><span class="text-orange">4 ผลลัพธ์</span> สำหรับหน้านี้</p>
			<hr>
			<p>ผลการค้นหาหน้า : <span class="text-orange">#บทความ</span> <br>
			จำนวน : 5 หัวข้อ</p>

			<div class="box-article-list">
				<?php for($i=1;$i<=5;$i++) { ?>
				<div class="article-list">
					<a href="article-detail.php">
						<span class="thumbnail" style="background-image:url('assets/img/img-article.jpg');"></span>
						<span class="text">
							<span class="hashtag">#เอกสารองค์กร</span>
							<span class="title">รู้จักกับสถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน)</span>
							วันที่ 13 มิถุนายน 2558 ถือเป็นวันครบรอบสี่ปีของสถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน) หรือ ThailandInstitute of Justice (TIJ) 
							<span class="date"><i class="icon icon-calendar"></i> <span class="text-orange">23.01.2019</span></span>
						</span>
					</a>
				</div>
				<?php } ?>
			</div>
		</div>
		
		
		<div class="article-footer">
			<div class="page-counter">
				หน้า 01/14
			</div>
			<div class="pagination">
				<a href="#"><i class="icon icon-angle-left"></i> ย้อนกลับ</a>
				<a class="active" href="#">1</a>
				<a href="#">2</a>
				<a href="#">3</a>
				<a href="#">ต่อไป <i class="icon icon-angle-right"></i></a>
			</div>
			<div class="page-jump">
				กดเพื่อดูหน้า
				<input type="text" class="form-input" placeholder="1">
				<button class="btn">กด</button>
			</div>
		</div>
	</div>
</section>

<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>