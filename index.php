<?php include('inc/header.php'); ?>
<div class="main-banner owl-carousel owl-theme">
	<div class="slide">
		<div class="container">
			<div class="slide-content">
				<div class="hashtag">#จดหมายข่าว</div>
				<h2>รู้จักกับ <strong>สถาบันเพื่อการยุติธรรมแห่งประเทศไทย</strong> (องค์การมหาชน)</h2>
				<p>Just Right จดหมายข่าวสถานบันเพื่อการยุติธรรมแห่งประเทศไทย ฉบับที่ 001 มิถุนายน 2558</p>
				<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
				<a href="#" class="readmore">อ่านต่อ <i class="icon icon-angle-right"></i></a>
			</div>
		</div>
		<img src="assets/img/main-banner.jpg" alt="">
	</div>
	<div class="slide"><a class="owl-video" href="https://www.youtube.com/watch?v=ldZAhpEoDLM"></a></div>
</div>

<section class="section new-article">
	<div class="container">
		<h2 class="section-title">บทความมาใหม่</h2>
		<div class="slide-article owl-carousel owl-theme">
			<div class="card card-article card-highlight-article" data-merge="2">
				<a href="article-detail.php"></a>
				<div class="card-image" style="background-image:url('assets/img/img-highlight-article.jpg');"></div>
				<div class="card-content">
					<div class="hashtag">#บทความ</div>
					<h3 class="card-title">รู้จักกับสถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน)</h3>
					<div class="card-footer">
						<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
						<a class="readmore" href="article-detail.php">อ่านต่อ <i class="icon icon-angle-right"></i></a>
					</div>
				</div>
			</div>
			<?php for($i=1;$i<=10;$i++) { ?>
			<div class="card card-article">
				<div class="card-image">
					<a href="article-detail.php" class="hover-img"> <img src="assets/img/img-article.jpg" class="img-responsive"></a>
				</div>
				<div class="card-header">
					<h3 class="card-title">รู้จักกับสถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน)</h3>
				</div>
				<div class="card-body">
					<p>รู้จักกับสถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน)</p>
				</div>
				<div class="card-footer">
					<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
					<a href="article-detail.php">อ่านต่อ</a>
				</div>
			</div>
			<?php } ?>
			
		</div>
		<a class="view-all float-right" href="article.php">ดูทั้งหมด <i></i></a>
	</div>
</section>

<section class="section highlight">
	<div class="container">
		<h2 class="section-title">ไฮไลท์</h2>
		<div class="highlight-article">
			<div class="columns">
				<ul class="tab column col-3 col-sm-12">
					<li class="tab-item active"><a href="javascript:;"><i class="icon icon-sheet"></i> บทความ</a></li>
					<li class="tab-item"><a href="javascript:;"><i class="icon icon-presentation"></i> อินโฟกราฟิก</a></li>
					<li class="tab-item"><a href="javascript:;"><i class="icon icon-open-mail"></i> จดหมายข่าว</a></li>
					<li class="tab-item"><a href="javascript:;"><i class="icon icon-vdo"></i> วีดีโอ</a></li>
					<li class="tab-item"><a href="javascript:;"><i class="icon icon-computer-book"></i> เอกสารวิชาการ</a></li>
					<li class="tab-item"><a href="javascript:;"><i class="icon icon-international-standard"></i> มาตรฐานระหว่างประเทศ</a></li>
					<li class="tab-item"><a href="javascript:;"><i class="icon icon-folder"></i> รายงานประจำปี</a></li>
					<li class="all-category"><a href="category.php">หมวดหมู่ทั้งหมด <span>+</span></a></li>
				</ul>
				<div class="highlight-article-list column col-9 col-sm-12">
					<!-- article -->
					<div class="tab-content">
						<div class="columns">
							<?php for($i=1;$i<=6;$i++) { ?>
							<div class="column col-4 col-sm-12 col-lg-6">
								<div class="card card-article">
									<div class="card-image">
										<a href="article-detail.php" class="hover-img"><img src="assets/img/img-article.jpg" class="img-responsive"></a>
									</div>
									<div class="card-header">
										<h3 class="card-title">รู้จักกับสถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน)</h3>
									</div>
									<div class="card-body">
										<p>รู้จักกับสถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน)</p>
									</div>
									<div class="card-footer">
										<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
										<a href="">อ่านต่อ</a>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
						<a class="view-all float-right" href="article.php">ดูทั้งหมด <i></i></a>
					</div>
					
					<!-- info -->
					<div class="tab-content">
						<div class="columns">
							<?php for($i=0;$i<=5;$i++) { ?>
							<?php 
								$title = array("Violence Cycle - NOV 2017","IBR From Victim to Convict - NOV 2017","Violence Opinion - NOV 2017","SUSO Rape Media - NOV 2017","Rape Stat - NOV 2017","SUSO รักนวลสงวนสิทธิ์ - NOV 2017");
							?>
							<div class="column col-4 col-lg-6 col-sm-12">
								<div class="card card-article">
									<div class="card-image">
										<a href="infographic-detail.php" class="hover-img"><img src="assets/img/article/infographic/0<?php echo $i+1;?>.jpg" class="img-responsive"></a>
									</div>
									<div class="card-header">
										<div class="hashtag">#อินโฟกราฟิก</div>
										<h3 class="card-title"><?php echo $title[$i]; ?></h3>
									</div>
									<div class="card-footer">
										<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
										<a href="">อ่านต่อ</a>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
						<a class="view-all float-right" href="infographic.php">ดูทั้งหมด <i></i></a>
					</div>

					<!-- newsletter -->
					<div class="tab-content">
						<div class="columns">
							<?php for($i=0;$i<=6;$i++) { ?>
							<?php 
								$month = array("มิถุนายน 2558","ตุลาคม 2558","พฤษภาคม 2559","มิถุนายน 2560","พฤศจิกายน 2560","พฤษภาคม 2561","สิงหาคม 2561");
							?>
							<div class="column col-3 col-lg-6 col-sm-12">
								<div class="card card-article">
									<div class="card-image">
										<a href="publication-detail.php" class="hover-img">
											<img src="assets/img/article/newsletter/0<?php echo $i+1;?>.jpg" class="img-responsive">
										</a>
									</div>
									<div class="card-header">
										<div class="hashtag">#จดหมายข่าว</div>
										<h3 class="card-title">TIJ QUARTERLY ฉบับที่ 00<?php echo $i+1;?> <?php echo $month[$i]; ?></h3>
									</div>
									<div class="card-footer">
										<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
						<a class="view-all float-right" href="newsletter.php">ดูทั้งหมด <i></i></a>
					</div>

					<!-- vdo -->
					<div class="tab-content">
						<div class="columns">
							<?php for($i=1;$i<=6;$i++) { ?>
							<div class="vdo-list column col-4 col-sm-12 col-lg-6">
								<div class="card card-article">
									<div class="card-image">
										<a href="vdo-detail.php" class="hover-img">
											<img src="assets/img/img-article.jpg" class="img-responsive">
										</a>
									</div>
									<div class="card-header">
										<div class="hashtag">#วิดีโอ</div>
										<h3 class="card-title">TIJ Borderless Youth Forum 2019 Teaser</h3>
									</div>
									<div class="card-footer">
										<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
						<a class="view-all float-right" href="vdo.php">ดูทั้งหมด <i></i></a>
					</div>

					<!-- publication -->
					<div class="tab-content">
						<div class="columns">
							<?php for($i=0;$i<=6;$i++) { ?>
							<?php 
								$title = array("The Model Prison","Assessment Tool - ข้อกำหนดแมนเดลา","EXPO 1st Seminar Report Community","TIJ Booklet 2018 on Dev Led Crime Prevention","Bangkok Dialogue RoL","Bangkok Rules Training","EXPO Pocket Guide");
							?>
							<div class="column col-3 col-lg-6 col-sm-12">
								<div class="card card-article">
									<div class="card-image">
										<a href="publication-detail.php" class="hover-img">
											<img src="assets/img/article/publication/0<?php echo $i+1;?>.jpg" class="img-responsive">
										</a>
									</div>
									<div class="card-header">
										<div class="hashtag">#สิ่งพิมพ์</div>
										<h3 class="card-title"><?php echo $title[$i]; ?></h3>
									</div>
									<div class="card-footer">
										<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
						<a class="view-all float-right" href="publication.php">ดูทั้งหมด <i></i></a>
					</div>

					<!-- statistics -->
					<div class="tab-content">
						<h2 class="text-grey">ไม่พบข้อมูล</h2>
						<!-- <a class="view-all float-right" href="statistic.php">ดูทั้งหมด <i></i></a> -->
					</div>

					<!-- research -->
					<div class="tab-content">
						<div class="columns">
							<?php for($i=0;$i<=6;$i++) { ?>
							<?php 
								$title = array("Women's Pathways to Prison in Kenya","เพศภาวะและเส้นทางสู่เรือนจำ","Victimization Experiences Among Young People In Custody : Systematic Review pf Methods and Tools","Violence against Children Survey","The Trial of Rape","Trafficking in persons from Cambodia, Lao PDR and Myanmar to Thailand","Criminal Justice Performance Indicators Expected Values and Gaps");
							?>
							<div class="column col-3 col-lg-6 col-sm-12">
								<div class="card card-article">
									<div class="card-image">
										<a href="publication-detail.php" class="hover-img">
											<img src="assets/img/article/research/0<?php echo $i+1;?>.jpg" class="img-responsive">
										</a>
									</div>
									<div class="card-header">
										<div class="hashtag">#สิ่งพิมพ์</div>
										<h3 class="card-title"><?php echo $title[$i]; ?></h3>
									</div>
									<div class="card-footer">
										<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
						<a class="view-all float-right" href="research.php">ดูทั้งหมด <i></i></a>
					</div>

					<!-- corporate material -->
					<div class="tab-content">
						<div class="columns">
							<?php for($i=0;$i<=4;$i++) { ?>
							<?php 
								$title = array("TIJ Booklet 2018 - EN","Annual Report 2016 - TH","Annual Report 2015 - TH","Annual Report 2014 - EN","Annual Report 2014 - TH");
							?>
							<div class="column col-3 col-lg-6 col-sm-12">
								<div class="card card-article">
									<div class="card-image">
										<a href="publication-detail.php" class="hover-img">
											<img src="assets/img/article/corporate-material/0<?php echo $i+1;?>.jpg" class="img-responsive">
										</a>
									</div>
									<div class="card-header">
										<div class="hashtag">#เอกสารองค์กร</div>
										<h3 class="card-title"><?php echo $title[$i]; ?></h3>
									</div>
									<div class="card-footer">
										<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
						<a class="view-all float-right" href="corporate-material.php">ดูทั้งหมด <i></i></a>
					</div>
				</div>
				
			</div>
			
		</div>
	</div>
</section>

<section class="section-subscribe">
	<div class="container">
		<div class="box-subscribe">
			<p>Get fresh perspectives<br>
			<span class="text-orange">from TIJ</span><br>
			straight to your inbox</p>
			<a data-fancybox data-type="ajax" data-src="popup/subscribe.php" class="btn">Subscribe</a>
		</div>
	</div>
</section>

<div class="pdpa-cookie">
	<div class="container page-center">
		<h3>ยอมรับนโยบายความเป็นส่วนตัว</h3>
		<div class="box-detail">
			<div class="detail">
				<p>เว็บไซต์นี้มีการใช้คุกกี้ (cookie) เพื่อพัฒนาประสบการณ์การใช้งานและเพิ่มความพึงพอใจต่อการได้รับการเสนอข้อมูลและเนื้อหาต่างๆ ให้ดียิ่งขึ้น โดยการเข้าใช้งานเว็บไซต์นี้ถือว่าท่านได้อนุญาตให้เราใช้คุกกี้ตามนโยบายคุกกี้ของเรา</p>
			</div>
			<div class="box-btn">
				<a href="#" class="btn btn-style">ตั้งค่าคุกกี้</a>
				<a href="#" class="btn btn-style">ยอมรับคุกกี้ทั้งหมด</a>
			</div>
		</div>
	</div>
</div>

<?php include('inc/javascript.php'); ?>
<script>
	$(document).ready(function () {
		indexEvent();
	});
</script>
<?php include('inc/footer.php'); ?>