	<footer>
		<div class="container">
			<a class="go-to-top btn" href="javascript:;">
				<i class="icon icon-angle-up"></i>
				Top
			</a>
			<nav class="footer-navigation">
				<div class="navigation">
					<a href="article.php" class="link">บทความ</a>
					<a href="infographic.php" class="link">อินโฟกราฟิก</a>
					<a href="newsletter.php" class="link">จดหมายข่าว</a>
					<a href="vdo.php" class="link">วีดีโอ</a>
					<a href="publication.php" class="link">เอกสารวิชาการ</a>
					<a href="international-standard.php" class="link">มาตรฐานระหว่างประเทศ</a>
					<a href="corporate-material.php" class="link">รายงานประจำปี</a>
				</div>
				<div class="social-media">
					SOCIAL MEDIA
					<a href="#"><i class="icon-facebook"></i></a>
					<a href="#"><i class="icon-twitter"></i></a>
					<a href="#"><i class="icon-youtube"></i></a>
				</div>
			</nav>
		</div>
		<div class="line"></div>
		<div class="container">
			<div class="footer-bottom">
				<div class="footer-detail">
					<p>สถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน)<br>
					93/1 อาคารจีพีเอฟ อาคารบี ชั้น 15-16 ถนนวิทยุ แขวงลุมพินี เขตปทุมวัน กรุงเทพฯ 10330</p>
					<p class="copyright">&copy; COPYRIGHT 2018, THAILAND INSTITUTE OF JUSTICE </p>
				</div>
				<a href="#" class="btn btn-outline main-website">กลับสู่เว็บไซต์หลัก</a>
			</div>
		</div>
		
	</footer>

</div>
</body>
</html>