<div class="box-main-menu">
	<div class="container">
		<nav class="navigation">
			<a href="index.php" class="logo"><img src="assets/img/logo.png" alt="TIJ"></a>
			<div class="navigation-top">
				<a href="article.php" class="link">บทความ</a>
				<a href="infographic.php" class="link">อินโฟกราฟิก</a>
				<a href="newsletter.php" class="link">จดหมายข่าว</a>
				<a href="vdo.php" class="link">วีดีโอ</a>
				<a href="publication.php" class="link">เอกสารวิชาการ</a>
				<a href="international-standard.php" class="link">มาตรฐานระหว่างประเทศ</a>
				<a href="corporate-material.php" class="link">รายงานประจำปี</a>
			</div>
		</nav>
		<div class="header-tool">
			<div class="menu-mobile">
				<i class="icon icon-hamburger"></i>
				<i class="icon icon-hamburger-open"></i>
			</div>
			<a href="#" class="btn-search"><i class="icon-search"></i> ค้นหา</a>
			<div class="btn-text-size">
				<a href="#" class="btn btn-text-normal active">A</a><a href="#" class="btn btn-text-large">A+</a>
			</div>
			<div class="btn-change-lang">
				<a href="index.php" class="btn active">TH</a><a href="index-en.php" class="btn">EN</a>
			</div>
		</div>
	</div>
</div>