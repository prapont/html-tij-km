<div class="box-main-menu">
	<div class="container">
		<nav class="navigation">
			<a href="index.php" class="logo"><img src="assets/img/logo.png" alt="TIJ"></a>
			<div class="navigation-top">
				<a href="http://tij.campaignserv.com/tij_km/en/article" class="link ">ARTICLE</a>
				<a href="http://tij.campaignserv.com/tij_km/en/infographic" class="link ">INFOGRAPHICS</a>
				<a href="http://tij.campaignserv.com/tij_km/en/newsletter" class="link ">NEWSLETTERS</a>
				<a href="http://tij.campaignserv.com/tij_km/en/video" class="link ">VIDEOS</a>
				<a href="http://tij.campaignserv.com/tij_km/en/publication" class="link ">KNOWLEDGE / PUBLICATIONS</a>
				<a href="http://tij.campaignserv.com/tij_km/en/not_found" class="link">INTERNATIONAL STANDARD</a>
				<a href="http://tij.campaignserv.com/tij_km/en/corporate_material" class="link ">ANNUAL REPORTS</a>
			</div>
		</nav>
		<div class="header-tool">
			<div class="menu-mobile">
				<i class="icon icon-hamburger"></i>
				<i class="icon icon-hamburger-open"></i>
			</div>
			<a href="#" class="btn-search"><i class="icon-search"></i> ค้นหา</a>
			<div class="btn-text-size">
				<a href="#" class="btn btn-text-normal active">A</a><a href="#" class="btn btn-text-large">A+</a>
			</div>
			<div class="btn-change-lang">
				<a href="index.php" class="btn">TH</a><a href="index-en.php" class="btn active">EN</a>
			</div>
		</div>
	</div>
</div>