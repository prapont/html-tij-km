<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>TIJ</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<link rel="stylesheet" href="assets/css/vendor/spectre.min.css">
	<link rel="stylesheet" href="assets/css/vendor/owlcarousel/owl.carousel.min.css">
	<link rel="stylesheet" href="assets/css/vendor/owlcarousel/owl.theme.default.min.css">
	<link rel="stylesheet" href="assets/css/vendor/jquery.fancybox.min.css">
	<link rel="stylesheet" href="assets/css/vendor/jscrollpane.css">
	<link rel="stylesheet" href="assets/js/wow_book/wow_book.css" type="text/css" />
	<link rel="stylesheet" href="assets/css/layout.min.css">
	<link rel="stylesheet" href="assets/css/pdpa.css">
</head>
<body>
<div class="wrapper">
	<header class="header">
		
		<?php include('navigation.php'); ?>
		
		<div class="box-search">
			<form action="search-result.php">
				<h2>ค้นหา</h2>
				<h3>เลือกหมวดหมู่</h3>
				<div class="category-filter">
					<?php for($i=0;$i<=7;$i++) { ?>
					<?php 
						$category = array("ทั้งหมด","บทความ","อินโฟกราฟิก","จดหมายข่าว","วีดีโอ","เอกสารวิชาการ","มาตราฐานระหว่างประเทศ","รายงานประจำปี");
					?>
					<div class="list">
						<div class="form-group">
							<label class="form-checkbox">
								<input type="checkbox">
								<i class="form-icon"></i> <?php echo $category[$i]; ?>
								<div class="bg"></div>
							</label>
						</div>
					</div>
					<?php } ?>
				</div>
				<div class="text-center">
					<input type="text" class="form-input" placeholder="พิมพ์คำค้นหา">
					<input type="submit" class="btn" value="ค้นหา">
				</div>
			</form>
		</div>
	</header>
	
