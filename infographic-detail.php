<?php include('inc/header.php'); ?>

<div class="breadcrumbs">
	<div class="container"><a href="index.php">หน้าหลัก</a><a href="infographic.php">อินโฟกราฟิก</a><span>Women Access to Justice - MAR 2018</span></div>
</div>

<div class="article-detail">
	<div class="container">
		<div class="columns">
			<div class="column col-8 col-md-12">
				<h1>Women Access to Justice - MAR 2018</h1> 
				<div class="article-slideshow infographic-slideshow">
					<div class="article-share">
						<a href="infographic.php" class="btn"><i class="icon icon-angle-left"></i></a>
						<div class="share">
							<div class="btn btn-grey"><i class="icon icon-share"></i></div>
							<a class="btn btn-grey" href="http://www.google.co.th"><i class="icon icon-facebook"></i></a>
							<a class="btn btn-grey" href="#"><i class="icon icon-twitter"></i></a>
						</div>
					</div>
					<div class="main-image">
						<img src="assets/img/article/infographic/women-access-to-justice/0.png" alt="">
					</div>
					<div class="owl-carousel owl-theme">
						<?php for($i=0;$i<=23;$i++) { ?>
						<div class="slide" data-img-path="assets/img/article/infographic/women-access-to-justice/<?php echo $i; ?>.png">
							<img src="assets/img/article/infographic/women-access-to-justice/<?php echo $i; ?>.png" alt="">
						</div>
						<?php } ?>
					</div>
				</div>
				<div class="article-info">
					<span class="date"><i class="icon icon-calendar"></i> <span class="text-orange">23.01.2019</span></span>
					<span class="view"><i class="icon icon-open-book"></i> <span class="text-orange">560</span> จำนวนผู้เข้าชม</span>
					<span class="author text-orange">โดย ดร.กิตติพงษ์ กิตยารักษ์</span>
				</div>
				<div class="detail">
					<p>วันที่ 13 มิถุนายน 2558 ถือเป็นวันครบรอบสี่ปีของสถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน) หรือ ThailandInstitute of Justice (TIJ)</p>
				</div>
				<div class="download">
					<h3 class="title-underline">เอกสารดาวน์โหลด</h3>
					<div class="owl-carousel owl-theme">
						<?php for($i=1;$i<=5;$i++) { ?>
						<div class="download-list">
							<div class="download-detail">
								<p>เอกสารเพิ่มเติม ฉบับที่ <?php echo $i; ?>.PDF</p>
								<div class="file-info">
									<div class="info-list">
										ขนาด
										<span class="text-orange">0.5 Mb</span>
									</div>
									<div class="info-list">
										ประเภทไฟล์
										<span class="text-orange">PDF.</span>
									</div>
									<div class="info-list">
										จำนวนดาวน์โหลด
										<span class="text-orange">12000</span>
									</div>
								</div>
							</div>
							<a href="#" class="btn">
								<i class="icon icon-download"></i>
								ดาวน์โหลด
							</a>
						</div>
						<?php } ?>
					</div>
				</div>
				<div class="tag">
					<a href="#">บทความ</a>
					<a href="#">กฎหมาย</a>
					<a href="#">ข่าวสาร</a>
					<a href="#">ข่าวสาร</a>
					<a href="#">ข่าวสาร</a>
					<a href="#">ความยุติธรรม</a>
				</div>
			</div>
			<div class="column col-4 col-md-12">
				<div class="related-article">
					<h3>อินโฟกราฟิกน่าสนใจ</h3>
					<div class="box-article-list">
						<div class="article-list">
							<a href="infographic-detail.php">
								<span class="thumbnail" style="background-image:url('assets/img/article/infographic/02.jpg');"></span>
								<span class="text">
									Violence Cycle - NOV 2017
									<span class="date"><i class="icon icon-calendar"></i> <span class="text-orange">23.01.2019</span></span>
								</span>
							</a>
						</div>
						<div class="article-list">
							<a href="infographic-detail.php">
								<span class="thumbnail" style="background-image:url('assets/img/article/infographic/03.jpg');"></span>
								<span class="text">
									IBR From Victim to Convict - NOV 2017
									<span class="date"><i class="icon icon-calendar"></i> <span class="text-orange">23.01.2019</span></span>
								</span>
							</a>
						</div>
						<div class="article-list">
							<a href="infographic-detail.php">
								<span class="thumbnail" style="background-image:url('assets/img/article/infographic/04.jpg');"></span>
								<span class="text">
									Violence Opinion - NOV 2017
									<span class="date"><i class="icon icon-calendar"></i> <span class="text-orange">23.01.2019</span></span>
								</span>
							</a>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
</div>

<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>