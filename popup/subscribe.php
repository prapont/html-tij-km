<div class="popup -subscribe">
	<div class="inner">
		<h2>SUBSCRIPTION</h2>

		<div class="form-style form-subscribe">
			<form action="http://tij.campaignserv.com/tij_km/subscription/ajax_subscribe" method="post" accept-charset="utf-8" id="frm_subscription" class="frm-subscription">
				<div style="display:none">
					<input type="hidden" name="tijkm2019_token" value="ba221f2bdd553720612bba5d669c5d40">
				</div>
				<div class="items">
					<div class="item form-group">
						<div class="col select-box">
							<label for="title">Title</label>
							<select class="select-style" name="prefix_name">
								<option value="1">Mr.</option>
								<option value="2">Ms.</option>
							</select>
							<!-- <p class="text-validate">Validate</p> -->
						</div>
					</div>
					<div class="item form-group">
						<div class="col">
							<label for="first-name">First name</label>
							<input type="text" autocomplete="off" name="firstname" id="first-name" class="inputtext-style" placeholder="First name">

						</div>
					</div>
					<div class="item form-group">
						<div class="col">
							<label for="last-name">Last name</label>
							<input type="text" autocomplete="off" name="lastname" id="last-name" class="inputtext-style" placeholder="Last name">

						</div>
					</div>
					<div class="item form-group">
						<div class="col">
							<label for="organization">Organization</label>
							<input type="text" name="organization" id="organization" class="inputtext-style" placeholder="Organization">

						</div>
					</div>
					<div class="item form-group">
						<div class="col">
							<label for="position">Position</label>
							<input type="text" name="position" id="position" class="inputtext-style" placeholder="Position">

						</div>
					</div>
					<div class="item form-group">
						<div class="col">
							<label for="email">E-mail</label>
							<input type="email" name="email" id="email" class="inputtext-style" placeholder="E-mail">
						</div>
					</div>
					<div class="item form-group">
						<div class="col">
							<label for="tel">Telephone</label>
							<input type="tel" name="telephone" id="tel" class="inputtext-style" placeholder="Telephone">
						</div>
					</div>

					<div class="item form-group">
						<div class="col">
							<label for=""></label>
							<script src="https://www.google.com/recaptcha/api.js?hl=en"></script>

							<div class="g-recaptcha" data-sitekey="6LcVm3oUAAAAAJMRgJXTgsF0ex6nSnl8pMMcjSbk">
								<div style="width: 304px; height: 78px;">
									<div>
										<iframe src="https://www.google.com/recaptcha/api2/anchor?ar=1&amp;k=6LcVm3oUAAAAAJMRgJXTgsF0ex6nSnl8pMMcjSbk&amp;co=aHR0cDovL3Rpai5jYW1wYWlnbnNlcnYuY29tOjgw&amp;hl=en&amp;v=v1557729121476&amp;size=normal&amp;cb=3n5rrpex3z7k" width="304" height="78" role="presentation" name="a-aha43oz31ny0" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe>
									</div>
									<textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid rgb(193, 193, 193); margin: 10px 25px; padding: 0px; resize: none; display: none;"></textarea>
								</div>
							</div>
							<input type="hidden" name="recaptcha">
						</div>
					</div>

					<div class="item form-group">
						<div class="col checkbox-style">
							<input type="checkbox" id="allow" name="agree">
							<label for="allow">I agree to receive emails from TIJ.</label>
						</div>
					</div>
					<div class="item">
						<div class="col">
							<button type="submit" class="btn-style">SUBMIT</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>