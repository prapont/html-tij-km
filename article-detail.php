<?php include('inc/header.php'); ?>

<div class="breadcrumbs">
	<div class="container"><a href="index.php">หน้าหลัก</a><a href="article.php">บทความ</a><span>รู้จักกับสถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน)</span></div>
</div>

<div class="article-detail">
	<div class="container">
		<div class="columns">
			<div class="column col-8 col-md-12">
				<h1>รู้จักกับสถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน)</h1> 
				<div class="article-slideshow">
					<div class="article-share">
						<a href="book.php" class="btn"><i class="icon icon-angle-left"></i></a>
						<div class="share">
							<div class="btn btn-grey"><i class="icon icon-share"></i></div>
							<a class="btn btn-grey" href="http://www.google.co.th"><i class="icon icon-facebook"></i></a>
							<a class="btn btn-grey" href="#"><i class="icon icon-twitter"></i></a>
						</div>
					</div>
					<div class="owl-carousel owl-theme">
						<div class="slide"><img src="assets/img/main-banner.jpg" alt=""></div>
						<div class="slide"><img src="assets/img/main-banner.jpg" alt=""></div>
					</div>
				</div>
				<div class="article-info">
					<span class="date"><i class="icon icon-calendar"></i> <span class="text-orange">23.01.2019</span></span>
					<span class="view"><i class="icon icon-open-book"></i> <span class="text-orange">560</span> จำนวนผู้เข้าชม</span>
					<span class="author text-orange">โดย ดร.กิตติพงษ์ กิตยารักษ์</span>
				</div>
				<div class="detail">
					<p>วันที่ 13 มิถุนายน 2558 ถือเป็นวันครบรอบสี่ปีของสถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน) หรือ ThailandInstitute of Justice (TIJ) ซึ่งเป็นองค์กรใหม่ที่สําคัญ มีภารกิจหลักในการส่งเสริมและสนับสนุนการอนุวัติข้อกําหนด สหประชาชาติในด้านงานกระบวนการยุติธรรมทางอาญาทั้งในประเทศและระดับภูมิภาค โดยเฉพาะอย่างยิ่งข้อกําหนดสหประชาชาติว่าด้วยการปฏิบัติต่อผู้ต้องขังหญิงในเรือนจําและมาตรการที่มิใช่การคุมขังสําหรับผู้กระทําความผิดหญิง หรือข้อกําหนดกรุงเทพซึ่งรัฐบาลไทยภายใต้การนําของพระเจ้าหลานเธอ พระองค์เจ้าพัชรกิติยาภา มีบทบาทริเริ่มพัฒนาขึ้น</p>
					<p>ตลอดเวลาที่ผ่านมา TIJ ได้ร่วมมือกับภาคีทั้งในและต่างประเทศ รวมถึงในภูมิภาคเอเชียแปซิฟิก และในกรอบประชาคมอาเซียน ดําเนินโครงการวิจัย ฝึกอบรมและเผยแพร่องค์ความรู้เพื่อพัฒนาศักยภาพระบบงานและบุคลากรกระบวนการยุติธรรม การส่งเสริมหลักนิติธรรมในทุกภาคส่วน และดําเนินกิจกรรมต่างๆ อันมีส่วนช่วยยกระดับภาพลักษณ์และผลักดันบทบาทของประเทศไทยในเวทีระหว่างประเทศ ดร.กิตติพงษ์ กิตยารักษ์ อดีตปลัดกระทรวงยุติธรรมได้เข้ารับตําแหน่ง ผู้อํานวยการสถาบันเพื่อการยุติธรรมแห่งประเทศไทย ในช่วงปีต้นปีที่ผ่านมา กําหนดวางเป้าหมายของสถาบันฯว่าจะมุ่งสู่การเป็นสถาบันสมทบในเครือข่ายของสหประชาชาติ (Programme Network Institutes – PNIs)แห่งแรกในภูมิภาคอาเซียน PNIs คือเครือข่ายสถาบันวิชาการและองค์การระหว่างประเทศ ซึ่งทําหน้าที่สนับสนุนงานวิชาการที่จําเป็นในการกําหนดนโยบาย และดําเนินภารกิจต่างๆ ของสหประชาชาติด้านการป้องกันอาชญากรรมและความยุติธรรมทางอาญา “หน่วยงานที่จะได้รับรองให้มีสถานะเป็นส่วนหนึ่งของเครือข่าย</p>
					<p>PNIs จะต้องได้รับการยอมรับในระดับนานาชาติ ในด้านความเป็นเลิศทางวิชาการ มีจุดเด่น(niche) และมีศักยภาพที่จะสนับสนุนภารกิจในด้านวิชาการได้ นอกจากนี้ยังต้องเป็นหน่วยงานที่มีบทบาทสําคัญในภูมิภาค และต้องมีการบริหารจัดการที่มีประสิทธิภาพสูง”</p>
					<p>ดร.กิตติพงษ์กล่าวสํานักงานของ TIJ ตั้งอยู่บนชั้น 15 และ 16 ของอาคารGPF (Government Pension Fund) บนถนนวิทยุ ติดกับสถานทูตสหรัฐฯ ซึ่งโดยที่ตั้งเองก็สะท้อนอัตลักษณ์ของสถาบันฯ ในฐานะที่เป็นองค์กรที่มีต้นกําเนิดจากภาคราชการ แต่กําลังอยู่ในช่วงเปลี่ยนผ่านเพื่อเป็นหน่วยงานระดับสากลได้เป็นอย่างดีบุคลากรของสถาบันฯ ก็ล้วนเป็นคนรุ่นใหม่ ที่มีความรู้ความสามารถสูงโดยเฉพาะทีมผู้บริหารไฟแรงที่ประกอบด้วยส่วนผสมอันลงตัวระหว่างผู้มีประสบการณ์ในงานราชการ นักวิชาการ</p>
				</div>
				<div class="gallery">
					<h3 class="title-underline">แกลเลอรี่</h3>
					<div class="gallery-list columns">
						<?php for($i=1;$i<=10;$i++) { ?>
						<div class="image-list column col-3 col-sm-6">
							<a class="hover-img" href="assets/img/thumb-gallery.jpg" data-fancybox="images">
								<img src="assets/img/thumb-gallery.jpg" alt="">
							</a>
						</div>
						<?php } ?>
					</div>
				</div>
				<div class="download">
					<h3 class="title-underline">เอกสารดาวน์โหลด</h3>
					<div class="owl-carousel owl-theme">
						<?php for($i=1;$i<=5;$i++) { ?>
						<div class="download-list">
							<div class="download-detail">
								<p>เอกสารเพิ่มเติม ฉบับที่ <?php echo $i; ?>.PDF</p>
								<div class="file-info">
									<div class="info-list">
										ขนาด
										<span class="text-orange">0.5 Mb</span>
									</div>
									<div class="info-list">
										ประเภทไฟล์
										<span class="text-orange">PDF.</span>
									</div>
									<div class="info-list">
										จำนวนดาวน์โหลด
										<span class="text-orange">12000</span>
									</div>
								</div>
							</div>
							<a href="#" class="btn">
								<i class="icon icon-download"></i>
								ดาวน์โหลด
							</a>
						</div>
						<?php } ?>
					</div>
				</div>
				<div class="tag">
					<a href="#">บทความ</a>
					<a href="#">กฎหมาย</a>
					<a href="#">ข่าวสาร</a>
					<a href="#">ข่าวสาร</a>
					<a href="#">ข่าวสาร</a>
					<a href="#">ความยุติธรรม</a>
				</div>
			</div>
			<div class="column col-4 col-md-12">
				<div class="related-article">
					<h3>บทความน่าสนใจ</h3>
					<div class="box-article-list">
						<?php for($i=1;$i<=8;$i++) { ?>
						<div class="article-list">
							<a href="article-detail.php">
								<span class="thumbnail" style="background-image:url('assets/img/img-article.jpg');"></span>
								<span class="text">
									รู้จักกับสถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน)
									<span class="date"><i class="icon icon-calendar"></i> <span class="text-orange">23.01.2019</span></span>
								</span>
							</a>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>


<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>