<?php include('inc/header.php'); ?>

<div class="breadcrumbs">
	<div class="container"><a href="index.php">หน้าหลัก</a><span>รายงานประจำปี</span></div>
</div>

<section class="section section-article-list">
	<div class="container">
		<h2 class="section-title">รายงานประจำปี</h2> 
		<div class="article-filter">
			จัดเรียง 
			<select name="" class="form-select">
				<option value="">รายงานประจำปีล่าสุด</option>
				<option value="">รายงานประจำปี 30 วันที่ผ่านมา</option>
			</select>
		</div>
		<div class="columns">
			<?php for($i=0;$i<=4;$i++) { ?>
			<?php 
				$title = array("TIJ Booklet 2018 - EN","Annual Report 2016 - TH","Annual Report 2015 - TH","Annual Report 2014 - EN","Annual Report 2014 - TH");
			?>
			<div class="column col-3 col-lg-6 col-sm-12">
				<div class="card card-article">
					<div class="card-image">
						<a href="publication-detail.php" class="hover-img">
							<img src="assets/img/article/corporate-material/0<?php echo $i+1;?>.jpg" class="img-responsive">
						</a>
					</div>
					<div class="card-header">
						<div class="hashtag">#รายงานประจำปี</div>
						<h3 class="card-title"><?php echo $title[$i]; ?></h3>
					</div>
					<div class="card-footer">
						<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="article-footer">
			<div class="page-counter">
				หน้า 01/1
			</div>
			<div class="pagination">
				<a class="active" href="#">1</a>
			</div>
			<div class="page-jump">
				กดเพื่อดูหน้า
				<input type="text" class="form-input" placeholder="1">
				<button class="btn">กด</button>
			</div>
		</div>
	</div>
</section>

<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>