<?php include('inc/header.php'); ?>

<div class="breadcrumbs">
	<div class="container"><a href="index.php">หน้าหลัก</a><span>หมวดหมู่</span></div>
</div>

<section class="section section-category">
	<div class="container">
		<h2 class="section-title">หมวดหมู่</h2> 
		<div class="owl-carousel owl-theme">
			<?php 
				$title = array("บทความ","อินโฟกราฟิก","จดหมายข่าว","วีดีโอ","เอกสารวิชาการ","มาตรฐานระหว่างประเทศ","รายงานประจำปี");
				$category = array("article","infographic","newsletter","vdo","publication","international-standard","corporate-material");
			?>
			<?php for($i=0;$i<=6;$i++) { ?>
			<div class="card card-article">
				<a href="<?php echo $category[$i]; ?>.php" class="hover-img">
					<span class="card-image">
						<img src="assets/img/category/<?php echo $category[$i]; ?>.jpg" class="img-responsive">
					</span>
					<span class="card-header">
						<h3 class="card-title"><?php echo $title[$i]; ?></h3>
						<span class="btn"><i class="icon icon-arrow-right"></i></span>
					</span>
				</a>
			</div>
			<?php } ?>
		</div>
	</div>
</section>

<?php include('inc/javascript.php'); ?>
<script>
	$(document).ready(function () {
		$(".owl-carousel").owlCarousel({
			items: 4,
			margin:10,
			nav:true
		});

	});
</script>
<?php include('inc/footer.php'); ?>