<?php include('inc/header.php'); ?>

<div class="breadcrumbs">
	<div class="container"><a href="index.php">หน้าหลัก</a><span>จดหมายข่าว</span></div>
</div>

<section class="section section-article-list">
	<div class="container">
		<h2 class="section-title">จดหมายข่าว</h2> 
		<div class="article-filter">
			จัดเรียง 
			<select name="" class="form-select">
				<option value="">จดหมายข่าวล่าสุด</option>
				<option value="">จดหมายข่าว 30 วันที่ผ่านมา</option>
			</select>
		</div>
		<div class="columns">
			<?php for($i=0;$i<=6;$i++) { ?>
			<?php 
				$month = array("มิถุนายน 2558","ตุลาคม 2558","พฤษภาคม 2559","มิถุนายน 2560","พฤศจิกายน 2560","พฤษภาคม 2561","สิงหาคม 2561");
			?>
			<div class="column col-3 col-lg-6 col-sm-12">
				<div class="card card-article">
					<div class="card-image">
						<a href="publication-detail.php" class="hover-img">
							<img src="assets/img/article/newsletter/0<?php echo $i+1;?>.jpg" class="img-responsive">
						</a>
					</div>
					<div class="card-header">
						<div class="hashtag">#จดหมายข่าว</div>
						<h3 class="card-title">TIJ QUARTERLY ฉบับที่ 00<?php echo $i+1;?> <?php echo $month[$i]; ?></h3>
					</div>
					<div class="card-footer">
						<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="article-footer">
			<div class="page-counter">
				หน้า 01/14
			</div>
			<div class="pagination">
				<a href="#"><i class="icon icon-angle-left"></i> ย้อนกลับ</a>
				<a class="active" href="#">1</a>
				<a href="#">2</a>
				<a href="#">3</a>
				<a href="#">ต่อไป <i class="icon icon-angle-right"></i></a>
			</div>
			<div class="page-jump">
				กดเพื่อดูหน้า
				<input type="text" class="form-input" placeholder="1">
				<button class="btn">กด</button>
			</div>
		</div>
	</div>
</section>

<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>