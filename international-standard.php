<?php include('inc/header.php'); ?>

<div class="breadcrumbs">
	<div class="container"><a href="index.php">หน้าหลัก</a><span>มาตรฐานระหว่างประเทศ</span></div>
</div>

<div class="page-404">
	<div class="container">
		<p>ขออภัย!
			<span>ไม่พบข้อมูล</span>
		</p>
		<a href="index.php" class="btn"><i class="icon icon-angle-left"></i> กลับสู่หน้าหลัก</a>
	</div>
</div>

<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>