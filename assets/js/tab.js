(function ($) {
	$.fn.tab = function (options) {
		var settings = $.extend({
			'ulTabName': 'tab',
			'tabContentName': 'tab-content',
			'tabPosition': 1,
			'showSubTab': false
		}, options);

		var root = $(this);

		root.find('.' + settings.ulTabName + ' li').eq(+settings.tabPosition + -1).addClass('active');
		root.find('.' + settings.tabContentName).hide();
		root.find('.' + settings.tabContentName).eq(+settings.tabPosition + -1).show().addClass('active');

		root.find('.' + settings.ulTabName + ' li').bind('click',function () {
			root.find('.' + settings.ulTabName + ' li').removeClass('active');
			$(this).addClass('active');
			root.find('.' + settings.tabContentName).hide();
			root.find('.' + settings.tabContentName).eq($(this).index()).show();
		});

		if(settings.showSubTab){
			root.find('.' + settings.tabContentName + ' .sub-tab-content').hide();
			root.find('.' + settings.tabContentName + ' .sub-tab-content').eq(0).show().addClass('active');

			root.find('.' + settings.tabContentName + ' .sub-tab-menu li').eq(0).addClass('active');
			root.find('.' + settings.tabContentName + ' .sub-tab-menu li').bind('click', function () {
				root.find('.' + settings.tabContentName + ' .sub-tab-menu li').removeClass('active');
				$(this).addClass('active');
				root.find('.' + settings.tabContentName + ' .sub-tab-content').hide();
				root.find('.' + settings.tabContentName + ' .sub-tab-content').eq($(this).index()).show();
			});
		}

	};
})(jQuery);