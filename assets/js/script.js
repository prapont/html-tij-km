jQuery(function ($) {
	SmoothScroll({
		animationTime: 800,
		stepSize: 150
	});

	$('.go-to-top').bind('click', function () {
		$('html, body').animate({ scrollTop: 0 }, 'fast');
		return false;
	});

	$('.btn-search').bind('click', function () {
		$('html').toggleClass('open-search');
		return false;
	});

	$('.btn-text-large').bind('click', function () {
		$('.btn-text-normal').removeClass('active');
		$(this).addClass('active');
		$('html').addClass('page-text-large');
		return false;
	});

	$('.btn-text-normal').bind('click', function () {
		$('.btn-text-large').removeClass('active');
		$(this).addClass('active');
		$('html').removeClass('page-text-large');
		return false;
	});

	$('.menu-mobile').bind('click', function () {
		$('.box-main-menu').toggleClass('open-menu');
		return false;
	});

	$('.jscrollpane').jScrollPane();
	$('.related-article .box-article-list').jScrollPane();

	articleEvent();
});

function flipbook() {
	var pathFile = $('#book').data('pdf-path');
	var bookOptions = {
		height: 500,
		width: 800,
		maxHeight: 600,
		centeredWhenClosed: true,
		hardcovers: true,
		pageNumbers: false,
		flipsound: false,
		flipSoundFile: '',
		// toolbar: "lastLeft, left, right, lastRight, toc, zoomin, zoomout, slideshow, flipsound, fullscreen, thumbnails, download",
		toolbar: "lastLeft, left, right, lastRight,  zoomin, zoomout, fullscreen",
		thumbnailsPosition: 'left',
		responsiveHandleWidth: 50,
		container: true,
		containerPadding: "20px",
		pdf: pathFile,
		zoomMax: 3,
		zoomStep: 1,
		zoomDuration: 100
	};

	if (!isMobile) {
		$('.pdf-viewer').hide();
		$('#book').wowBook(bookOptions); // create the book
	} else {
		$('.pdf-viewer').show();
		$('#book').hide();
	}
}

function indexEvent(){
	var mainBanner = $(".main-banner");
	mainBanner.owlCarousel({
		items: 1,
		video: false,
		videoWidth: false,
		videoHeight: false,
		autoplay:true,
		autoplayTimeout: 6000,
		autoplayHoverPause: false,
		loop:true,
		mouseDrag: false
	});

	var trigger = false;
	mainBanner.on('changed.owl.carousel', function (event) {
		if(!trigger){
			setTimeout(function () {
				$('.main-banner .owl-item.active .owl-video-play-icon').trigger('click');
			}, 100);
			trigger = true;
		}
	});

	$(".slide-article").owlCarousel({
		items: 5,
		dots: false,
		nav: true,
		mouseDrag: false,
		responsive: {
			0: {
				items: 2
			},
			768: {
				items: 3
			},
			1024: {
				items: 5
			}
		}
	});

	$(".highlight-article").tab();
}

function articleEvent(){
	if ($('.article-slideshow').hasClass('infographic-slideshow')) {
		var imgPath;
		$('.article-slideshow.infographic-slideshow .slide').bind('click', function () {
			imgPath = $(this).data('img-path');
			$('.main-image img').attr('src', imgPath);
		});
		
		$('.article-slideshow.infographic-slideshow .owl-carousel').owlCarousel({
			items: 4,
			slideBy: 4,
			margin: 5,
			nav: true,
			dots: false,
			mouseDrag: false
		});

	} else {
		$('.article-slideshow .owl-carousel').owlCarousel({
			items: 1,
			mouseDrag: false
		});
	}
	

	$('.download .owl-carousel').owlCarousel({
		items: 2,
		margin: 20,
		mouseDrag: false,
		responsive: {
			0: {
				items: 1
			},
			1024: {
				items: 2
			}
		}
	});

	var galleryCount = $('.gallery .image-list').length;
	$('.gallery .image-list').eq(3).find('a').append('<div class="image-counter"><span>' + (galleryCount - 4) + '+</div>');
}
