<?php include('inc/header.php'); ?>

<div class="breadcrumbs">
	<div class="container"><a href="index.php">หน้าหลัก</a><span>สถิติ</span></div>
</div>

<section class="section section-article-list">
	<div class="container">
		<h2 class="section-title">สถิติ</h2> 
		<div class="article-filter">
			จัดเรียง 
			<select name="" class="form-select">
				<option value="">สถิติล่าสุด</option>
				<option value="">สถิติ 30 วันที่ผ่านมา</option>
			</select>
		</div>
		<h2 class="text-grey">ไม่พบข้อมูล</h2>
		<div class="article-footer">
			<div class="page-counter">
				หน้า 01/14
			</div>
			<div class="pagination">
				<a href="#"><i class="icon icon-angle-left"></i> ย้อนกลับ</a>
				<a class="active" href="#">1</a>
				<a href="#">2</a>
				<a href="#">3</a>
				<a href="#">ต่อไป <i class="icon icon-angle-right"></i></a>
			</div>
			<div class="page-jump">
				กดเพื่อดูหน้า
				<input type="text" class="form-input" placeholder="1">
				<button class="btn">กด</button>
			</div>
		</div>
	</div>
</section>

<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>