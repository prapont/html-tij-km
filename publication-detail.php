<?php include('inc/header.php'); ?>

<div class="breadcrumbs">
	<div class="container"><a href="index.php">หน้าหลัก</a><a href="publication.php">เอกสารวิชาการ</a><span>JUST RIGHT ฉบับที่ 001 มิถุนายน 2558</span></div>
</div>

<div class="article-detail">
	<div class="container">
		<div class="columns">
			<div class="column col-8 col-md-12">
				<h1>JUST RIGHT ฉบับที่ 001 มิถุนายน 2558</h1> 

				<div class="book-container">
					<div class="article-share">
						<a href="book.php" class="btn"><i class="icon icon-angle-left"></i></a>
						<div class="share">
							<div class="btn btn-grey"><i class="icon icon-share"></i></div>
							<a class="btn btn-grey" href="#"><i class="icon icon-facebook"></i></a>
							<a class="btn btn-grey" href="#"><i class="icon icon-twitter"></i></a>
						</div>
					</div>
					<div id="book" data-pdf-path="assets/pdf/just-right-001.pdf"></div>
					<iframe class="pdf-viewer" width="100%" height="500" src="assets/pdf-viewer/#../pdf/just-right-001.pdf" frameborder="0" allowfullscreen webkitallowfullscreen></iframe>
				</div>

				<div class="article-info">
					<span class="date"><i class="icon icon-calendar"></i> <span class="text-orange">23.01.2019</span></span>
					<span class="view"><i class="icon icon-open-book"></i> <span class="text-orange">560</span> จำนวนผู้เข้าชม</span>
					<span class="author text-orange">โดย ดร.กิตติพงษ์ กิตยารักษ์</span>
				</div>

				<div class="book-info">
					<div class="columns col-gapless">		
						<div class="cover column col-4 col-sm-12">
							<div class="card card-article">
								<div class="card-image">
									<img src="assets/img/img-book.jpg" class="img-responsive">
								</div>
								<div class="card-header">
									<h3 class="card-title">JUST RIGHT ฉบับที่ 001 มิถุนายน 2558</h3>
								</div>
								<div class="card-footer">
									<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
								</div>
							</div>
						</div>
						<div class="column col-8 col-sm-12">
							<div class="detail">
								<dl>
									<dt>เดือน/ปีที่พิมพ์  :</dt>
									<dd>กันยายน 2561</dd>
									<dt>บทนำ :</dt>
									<dd>วันที่ 13 มิถุนายน 2558 ถือเป็นวันครบรอบสี่ปีของสถาบันเพื่อการยุติธรรม แห่งประเทศไทย (องค์การมหาชน) หรือ ThailandInstitute of Justice (TIJ) ซึ่งเป็นองค์กรใหม่ที่สําคัญ มีภารกิจหลักในการส่งเสริมและ สนับสนุนการอนุวัติ ข้อกําหนด สหประชาชาติในด้านงานกระบวนการยุติธรรมทางอาญาทั้งใน ประเทศและระดับภูมิภาค โดยเฉพาะอย่างยิ่งข้อกําหนดสหประชาชาติ ว่าด้วยการปฏิบัติต่อผู้ต้องขังหญิงในเรือนจําและมาตรการที่มิใช่การคุมขัง.....</dd>
									<dt>จำนวนที่พิมพ์ :</dt>
									<dd>1 เล่ม</dd>
									<dt>ผู้เขียน/บรรณาธิการ  :</dt>
									<dd>ดร.กิตติพงษ์ กิตยารักษ์</dd>
									<dt>หน่วยงาน :</dt>
									<dd>สถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน)</dd>
									<dt>สำนักพิมพ์ :</dt>
									<dd>-</dd>
									<dt>หมวดหมู่หนังสือ :</dt>
									<dd>สิ่งพิมพ์</dd>
								</dl>
								<a href="#" class="btn">ดาวน์โหลด <i class="icon icon-download"></i></a>
							</div>	
						</div>
					</div>
				</div>

				<div class="tag">
					<a href="#">บทความ</a>
					<a href="#">กฎหมาย</a>
					<a href="#">ข่าวสาร</a>
					<a href="#">ความยุติธรรม</a>
				</div>
			</div>
			<div class="column col-4 col-md-12">
				<div class="related-article">
					<h3>สิ่งพิมพ์อื่นที่น่าสนใจ</h3>
					<div class="columns">
						<?php for($i=1;$i<=4;$i++) { ?>
						<div class="column col-6">
							<div class="card card-article">
								<div class="card-image">
									<a href="book-detail.php" class="hover-img"><img src="assets/img/img-book.jpg" class="img-responsive"></a>
								</div>
								<div class="card-header">
									<div class="hashtag">#สิ่งพิมพ์</div>
									<h3 class="card-title">JUST RIGHT ฉบับที่ 00<?php echo $i; ?> มิถุนายน 2558</h3>
								</div>
								<div class="card-footer">
									<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
								</div>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>


<?php include('inc/javascript.php'); ?>
<script>
	$(document).ready(function () {
		flipbook();
	});
</script>
<?php include('inc/footer.php'); ?>