<?php include('inc/header.php'); ?>

<div class="breadcrumbs">
	<div class="container"><a href="index.php">หน้าหลัก</a><a href="vdo.php">วิดีโอ</a><span>JUST RIGHT ฉบับที่ 001 มิถุนายน 2558</span></div>
</div>

<div class="article-detail">
	<div class="container">
		<div class="columns">
			<div class="column col-8 col-md-12">
				<h1>TIJ Borderless Youth Forum 2019 Teaser</h1> 

				<div class="vdo-container">
					<iframe src="https://www.youtube.com/embed/lT9Mj7BZuA4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>

				<div class="article-info">
					<span class="date"><i class="icon icon-calendar"></i> <span class="text-orange">23.01.2019</span></span>
					<span class="view"><i class="icon icon-open-book"></i> <span class="text-orange">560</span> จำนวนผู้เข้าชม</span>
					<span class="author text-orange">โดย ดร.กิตติพงษ์ กิตยารักษ์</span>
				</div>
				
				<div class="download">
					<h3 class="title-underline">เอกสารดาวน์โหลด</h3>
					<div class="owl-carousel owl-theme">
						<?php for($i=1;$i<=5;$i++) { ?>
						<div class="download-list">
							<div class="download-detail">
								<p>เอกสารเพิ่มเติม ฉบับที่ <?php echo $i; ?>.PDF</p>
								<div class="file-info">
									<div class="info-list">
										ขนาด
										<span class="text-orange">0.5 Mb</span>
									</div>
									<div class="info-list">
										ประเภทไฟล์
										<span class="text-orange">PDF.</span>
									</div>
									<div class="info-list">
										จำนวนดาวน์โหลด
										<span class="text-orange">12000</span>
									</div>
								</div>
							</div>
							<a href="#" class="btn">
								<i class="icon icon-download"></i>
								ดาวน์โหลด
							</a>
						</div>
						<?php } ?>
					</div>
				</div>
				
				<div class="tag">
					<a href="#">บทความ</a>
					<a href="#">กฎหมาย</a>
					<a href="#">ข่าวสาร</a>
					<a href="#">ความยุติธรรม</a>
				</div>
			</div>
			<div class="column col-4 col-md-12">
				<div class="related-article">
					<h3>วิดีโออื่นที่น่าสนใจ</h3>
					<div class="columns">
						<div class="box-article-list">
							<?php for($i=1;$i<=8;$i++) { ?>
							<div class="article-list">
								<a href="vdo-detail.php">
									<span class="thumbnail" style="background-image:url('assets/img/img-article.jpg');"></span>
									<span class="text">
										รู้จักกับสถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน)
										<span class="date"><i class="icon icon-calendar"></i> <span class="text-orange">23.01.2019</span></span>
									</span>
								</a>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>


<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>