<?php include('inc/header.php'); ?>

<div class="breadcrumbs">
	<div class="container"><a href="index.php">หน้าหลัก</a><span>บทความ</span></div>
</div>

<section class="section section-article-list">
	<div class="container">
		<h2 class="section-title">บทความ</h2> 
		<div class="article-filter">
			จัดเรียง 
			<select name="" class="form-select">
				<option value="">บทความล่าสุด</option>
				<option value="">บทความ 30 วันที่ผ่านมา</option>
			</select>
		</div>
		<div class="columns">
			<div class="column col-6 col-lg-6 col-sm-12">
				<div class="card card-article card-highlight-article">
					<a href="article-detail.php"></a>
					<div class="card-image" style="background-image:url('assets/img/img-highlight-article.jpg');"></div>
					<div class="card-content">
						<div class="hashtag">#บทความ</div>
						<h3 class="card-title">รู้จักกับสถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน)</h3>
						<div class="card-footer">
							<p class="date"><i class="icon-calendar"></i> 23.01.2562</p>
							<a class="readmore" href="">อ่านต่อ <i class="icon-angle-right"></i></a>
						</div>
					</div>
				</div>
			</div>
			<?php for($i=1;$i<=6;$i++) { ?>
			<div class="column col-3 col-lg-6 col-sm-12">
				<div class="card card-article">
					<div class="card-image">
						<a href="article-detail.php" class="hover-img"><img src="assets/img/img-article.jpg" class="img-responsive"></a>
					</div>
					<div class="card-header">
						<div class="hashtag">#บทความ</div>
						<h3 class="card-title">รู้จักกับสถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน)</h3>
					</div>
					<div class="card-body">
						<p>รู้จักกับสถาบันเพื่อการยุติธรรมแห่งประเทศไทย (องค์การมหาชน)</p>
					</div>
					<div class="card-footer">
						<p class="date"><i class="icon icon-calendar"></i> 23.01.2562</p>
						<a href="">อ่านต่อ</a>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="article-footer">
			<div class="page-counter">
				หน้า 01/14
			</div>
			<div class="pagination">
				<a href="#"><i class="icon icon-angle-left"></i> ย้อนกลับ</a>
				<a class="active" href="#">1</a>
				<a href="#">2</a>
				<a href="#">3</a>
				<a href="#">ต่อไป <i class="icon icon-angle-right"></i></a>
			</div>
			<div class="page-jump">
				กดเพื่อดูหน้า
				<input type="text" class="form-input" placeholder="1">
				<button class="btn">กด</button>
			</div>
		</div>
	</div>
</section>

<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>